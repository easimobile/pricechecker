package com.easipos.pricechecker

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import androidx.core.app.TaskStackBuilder
import androidx.lifecycle.LifecycleObserver
import com.easipos.pricechecker.activities.main.MainActivity
import com.easipos.pricechecker.activities.splash.SplashActivity
import com.easipos.pricechecker.di.modules.*
import com.easipos.pricechecker.managers.UserManager
import com.easipos.pricechecker.tools.LocaleManager
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.pixplicity.easyprefs.library.Prefs
import io.github.anderscheow.library.CustomApplication
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware

class Easi : CustomApplication(), LifecycleObserver, KodeinAware {

    companion object {
        val UA = "Android/" + BuildConfig.FLAVOR + BuildConfig.VERSION_CODE + "_" + BuildConfig.BUILD_TYPE +
                " (" +
                Build.MANUFACTURER + " " +
                Build.MODEL + " " +
                Build.VERSION.RELEASE + " " +
                Build.VERSION_CODES::class.java.fields[Build.VERSION.SDK_INT].name +
                ")"
        const val API_ENDPOINT = BuildConfig.API_DOMAIN
        const val API_AUTHORISATION = ""
        const val DATABASE_NAME = "hualala-room"

        var activities = ArrayList<Activity>()

        var isActivityVisible = false

        @SuppressLint("StaticFieldLeak")
        var instance: Easi? = null

        lateinit var localeManager: LocaleManager
    }

    // Use to generate in-app notification
    var currentActivity: Activity? = null

    // Use mainActivity to refresh contents
    var mainActivity: MainActivity? = null
        private set

    override fun getCommonModule(): Kodein.Module = CommonModule(this).provideModule()

    override fun getActivityModule(): Kodein.Module = ActivityModule().provideModule()

    override fun getFragmentModule(): Kodein.Module = FragmentModule().provideModule()

    override fun importAdditionalModule(builder: Kodein.MainBuilder) {
        builder.apply {
            import(ApiModule(UA, API_ENDPOINT, API_AUTHORISATION).provideModule())
            import(DatabaseModule(this@Easi, DATABASE_NAME).provideModule())
        }
    }

    override fun attachBaseContext(base: Context?) {
        localeManager = LocaleManager(base!!)
        super.attachBaseContext(localeManager.setLocale(base))
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        setupConfiguration()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localeManager.setLocale(this)
    }

    fun logout() {
        UserManager.token = null
    }

    fun restartAndGotoOnboarding() {
        Logger.d("restartAndGotoOnboarding")
        logout()
        TaskStackBuilder.create(this)
            .addNextIntentWithParentStack(SplashActivity.newIntent(this, true))
            .startActivities()
    }

    private fun setupLifecycleCallback() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {
            }

            override fun onActivityResumed(activity: Activity?) {
                isActivityVisible = true
                currentActivity = activity
            }

            override fun onActivityStarted(activity: Activity?) {
            }

            override fun onActivityDestroyed(activity: Activity?) {
                activity?.let {
                    activities.remove(activity)
                }
                if (activity is MainActivity) {
                    mainActivity = null
                }
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
            }

            override fun onActivityStopped(activity: Activity?) {
            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                activity?.let {
                    activities.add(activity)
                }
                if (activity is MainActivity) {
                    mainActivity = activity
                }
            }
        })
    }

    private fun setupConfiguration() {
        setupLifecycleCallback()
        setupFirebaseCrashlytics()
        setupLogger()
        setupPrefs()
    }

    private fun setupLogger() {
        val formatStrategy = PrettyFormatStrategy.newBuilder()
            .showThreadInfo(false)
            .build()
        Logger.addLogAdapter(object : AndroidLogAdapter(formatStrategy) {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
    }

    private fun setupFirebaseCrashlytics() {
        if (BuildConfig.DEBUG.not()) {
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        }
    }

    private fun setupPrefs() {
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }
}

package com.easipos.pricechecker.repositories.precheck

import com.easipos.pricechecker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.pricechecker.datasource.DataFactory
import com.easipos.pricechecker.models.Result

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}

package com.easipos.pricechecker.repositories.checker

import com.easipos.pricechecker.models.ProductPricing
import com.easipos.pricechecker.models.Result

interface CheckerRepository {

    suspend fun getProductPricing(barcode: String): Result<ProductPricing>
}

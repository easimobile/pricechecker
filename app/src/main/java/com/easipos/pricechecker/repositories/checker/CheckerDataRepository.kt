package com.easipos.pricechecker.repositories.checker

import com.easipos.pricechecker.datasource.DataFactory
import com.easipos.pricechecker.mapper.ProductPricingMapper
import com.easipos.pricechecker.models.ProductPricing
import com.easipos.pricechecker.models.Result

class CheckerDataRepository(private val dataFactory: DataFactory) : CheckerRepository {

    private val productPricingMapper by lazy { ProductPricingMapper() }

    override suspend fun getProductPricing(barcode: String): Result<ProductPricing> =
        dataFactory.createCheckerDataSource()
            .getProductPricing(barcode, productPricingMapper)
}

package com.easipos.pricechecker.repositories.precheck

import com.easipos.pricechecker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.pricechecker.models.Result

interface PrecheckRepository {

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}

package com.easipos.pricechecker.models

data class Auth(val token: String)
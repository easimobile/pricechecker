package com.easipos.pricechecker.models

data class ProductPricing(
    val barcode: String,
    val prodCd: String,
    val prodNm: String,
    val price: Double,
    val promoMsg: String
)
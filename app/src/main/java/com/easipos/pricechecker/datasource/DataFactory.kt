package com.easipos.pricechecker.datasource

import android.app.Application
import com.easipos.pricechecker.api.services.Api
import com.easipos.pricechecker.datasource.checker.CheckerDataSource
import com.easipos.pricechecker.datasource.checker.CheckerDataStore
import com.easipos.pricechecker.datasource.precheck.PrecheckDataSource
import com.easipos.pricechecker.datasource.precheck.PrecheckDataStore
import com.easipos.pricechecker.room.RoomService
import io.github.anderscheow.library.executor.PostExecutionThread
import io.github.anderscheow.library.executor.ThreadExecutor

class DataFactory(private val application: Application,
                  private val api: Api,
                  private val roomService: RoomService,
                  private val threadExecutor: ThreadExecutor,
                  private val postExecutionThread: PostExecutionThread
) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api)

    fun createCheckerDataSource(): CheckerDataStore =
        CheckerDataSource(api)
}

package com.easipos.pricechecker.datasource.checker

import com.easipos.pricechecker.mapper.ProductPricingMapper
import com.easipos.pricechecker.models.ProductPricing
import com.easipos.pricechecker.models.Result

interface CheckerDataStore {

    suspend fun getProductPricing(barcode: String, productPricingMapper: ProductPricingMapper): Result<ProductPricing>
}

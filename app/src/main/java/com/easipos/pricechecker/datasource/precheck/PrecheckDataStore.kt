package com.easipos.pricechecker.datasource.precheck

import com.easipos.pricechecker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.pricechecker.models.Result

interface PrecheckDataStore {

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}

package com.easipos.pricechecker.datasource.precheck

import com.easipos.pricechecker.api.misc.parseException
import com.easipos.pricechecker.api.misc.parseResponse
import com.easipos.pricechecker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.pricechecker.api.services.Api
import com.easipos.pricechecker.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PrecheckDataSource(private val api: Api) : PrecheckDataStore {

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.checkVersion(model.toFormDataBuilder().build())
                parseResponse(response) {
                    it
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}

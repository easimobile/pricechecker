package com.easipos.pricechecker.datasource.checker

import com.easipos.pricechecker.api.misc.parseException
import com.easipos.pricechecker.api.misc.parseResponse
import com.easipos.pricechecker.api.services.Api
import com.easipos.pricechecker.mapper.ProductPricingMapper
import com.easipos.pricechecker.models.ProductPricing
import com.easipos.pricechecker.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CheckerDataSource(private val api: Api) : CheckerDataStore {

    override suspend fun getProductPricing(barcode: String, productPricingMapper: ProductPricingMapper): Result<ProductPricing> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getProductPricing(barcode)
                parseResponse(response) {
                    productPricingMapper.transform(it)
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}

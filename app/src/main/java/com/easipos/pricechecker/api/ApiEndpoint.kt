package com.easipos.pricechecker.api

object ApiEndpoint {

    const val CHECK_VERSION = "ddcard/version/check"

    const val GET_PRODUCT_PRICING = "getProductPricing"
}

package com.easipos.pricechecker.api.services

import com.easipos.pricechecker.api.ApiEndpoint
import com.easipos.pricechecker.api.misc.ResponseModel
import com.easipos.pricechecker.api.responses.checker.ProductPricingResponseModel
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    suspend fun checkVersion(@Body body: RequestBody): ResponseModel<Boolean>

    @GET(ApiEndpoint.GET_PRODUCT_PRICING)
    suspend fun getProductPricing(@Query("barcode") barcode: String): ResponseModel<ProductPricingResponseModel>
}

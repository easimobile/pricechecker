package com.easipos.pricechecker.api.responses.checker

import com.google.gson.annotations.SerializedName

data class ProductPricingResponseModel(
    @SerializedName("BARCODE")
    val barcode: String?,

    @SerializedName("PROD_CD")
    val prodCd: String?,

    @SerializedName("PROD_NM")
    val prodNm: String?,

    @SerializedName("PRICE_1")
    val price: String?,

    @SerializedName("PROMO_MSG")
    val promoMsg: String?
)
package com.easipos.pricechecker.event_bus

data class NotificationCount(val count: Int)
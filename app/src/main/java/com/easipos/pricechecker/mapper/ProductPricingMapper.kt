package com.easipos.pricechecker.mapper

import com.easipos.pricechecker.api.responses.checker.ProductPricingResponseModel
import com.easipos.pricechecker.models.ProductPricing

class ProductPricingMapper : Mapper<ProductPricing, ProductPricingResponseModel>() {

    override fun transform(item: ProductPricingResponseModel): ProductPricing {
        return ProductPricing(
            barcode = item.barcode ?: "",
            prodCd = item.prodCd ?: "",
            prodNm = item.prodNm ?: "",
            price = item.price?.toDoubleOrNull() ?: 0.0,
            promoMsg = item.promoMsg ?: ""
        )
    }
}
package com.easipos.pricechecker.base

import android.app.Application
import com.easipos.pricechecker.Easi
import io.github.anderscheow.library.viewModel.PagingWithoutLocalAndroidViewModel
import org.kodein.di.KodeinAware

abstract class CustomPagingWithoutLocalAndroidViewModel<in Args, Key, Value>(application: Application)
    : PagingWithoutLocalAndroidViewModel<Args, Key, Value>(application), KodeinAware {

    override val kodein by (application as Easi).kodein
}
package com.easipos.pricechecker.base

import io.github.anderscheow.library.appCompat.fragment.BaseFragment
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein

abstract class CustomBaseFragment : BaseFragment(), KodeinAware {

    private val _kodein: Kodein by closestKodein()

    override val kodein = Kodein.lazy {
        extend(_kodein)
    }
}

package com.easipos.pricechecker.base

import android.content.Context
import com.easipos.pricechecker.Easi
import io.github.anderscheow.library.appCompat.activity.BaseAppCompatActivity
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

abstract class CustomBaseAppCompatActivity : BaseAppCompatActivity(), KodeinAware {

    private val _kodein: Kodein by closestKodein()

    override val kodein = Kodein.lazy {
        extend(_kodein)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(Easi.localeManager.setLocale(base))
    }
}

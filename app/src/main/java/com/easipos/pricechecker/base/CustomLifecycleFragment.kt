package com.easipos.pricechecker.base

import io.github.anderscheow.library.appCompat.fragment.LifecycleFragment
import io.github.anderscheow.library.viewModel.BaseAndroidViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein

abstract class CustomLifecycleFragment<VM : BaseAndroidViewModel<*>>
    : LifecycleFragment<VM>(), KodeinAware {

    private val _kodein: Kodein by closestKodein()

    override val kodein = Kodein.lazy {
        extend(_kodein)
    }
}

package com.easipos.pricechecker.base

import android.app.Application
import com.easipos.pricechecker.Easi
import io.github.anderscheow.library.viewModel.BaseAndroidViewModel
import org.kodein.di.KodeinAware

abstract class CustomBaseAndroidViewModel<in Args>(application: Application)
    : BaseAndroidViewModel<Args>(application), KodeinAware {

    override val kodein by (application as Easi).kodein
}
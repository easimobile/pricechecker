package com.easipos.pricechecker.tools

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import com.pixplicity.easyprefs.library.Prefs
import java.util.*

class LocaleManager(context: Context) {

    val language: String?
        get() = Preference.prefLanguageCode

    init {
        Prefs.Builder()
            .setContext(context)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(context.packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }

    fun setLocale(c: Context): Context {
        return updateResources(c, language)
    }

    fun setNewLocale(c: Context, language: String): Context {
        persistLanguage(language)
        return updateResources(c, language)
    }

    private fun persistLanguage(language: String) {
        Preference.prefLanguageCode = language
    }

    private fun updateResources(context: Context, language: String?): Context {
        var newContext = context
        val locale = Locale(language ?: "en")
        Locale.setDefault(locale)

        val res = newContext.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        newContext = newContext.createConfigurationContext(config)
        return newContext
    }
}
package com.easipos.pricechecker.tools

import com.easipos.pricechecker.BuildConfig
import com.pixplicity.easyprefs.library.Prefs

object Preference {

    private const val PREF_LANGUAGE_CODE = "PREF_LANGUAGE_CODE"
    private const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
    private const val PREF_IS_LOGGED_IN = "PREF_IS_LOGGED_IN"
    private const val PREF_CUSTOM_BASE_URL = "PREF_CUSTOM_BASE_URL"
    private const val PREF_IDLE_TIME = "PREF_IDLE_TIME"

    var prefLanguageCode: String
        get() = Prefs.getString(PREF_LANGUAGE_CODE, "en")
        set(languageCode) = Prefs.putString(PREF_LANGUAGE_CODE, languageCode)

    var prefAccessToken: String
        get() = Prefs.getString(PREF_ACCESS_TOKEN, "")
        set(accessToken) = Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    var prefIsLoggedIn: Boolean
        get() = Prefs.getBoolean(PREF_IS_LOGGED_IN, false)
        set(isLoggedIn) = Prefs.putBoolean(PREF_IS_LOGGED_IN, isLoggedIn)

    var prefCustomBaseUrl: String
        get() = Prefs.getString(PREF_CUSTOM_BASE_URL, BuildConfig.API_DOMAIN)
        set(url) = Prefs.putString(PREF_CUSTOM_BASE_URL, url)

    var prefIdleTime: Int
        get() = Prefs.getInt(PREF_IDLE_TIME, 0)
        set(idleTime) = Prefs.putInt(PREF_IDLE_TIME, idleTime)

    fun logout() {
        prefIsLoggedIn = false

        Prefs.remove(PREF_ACCESS_TOKEN)
    }
}
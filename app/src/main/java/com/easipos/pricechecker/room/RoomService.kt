package com.easipos.pricechecker.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.easipos.pricechecker.models.Notification

@Database(
        entities = [
            Notification::class
        ],
        version = 1,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class RoomService : RoomDatabase() {

    abstract fun notificationDao(): NotificationDao
}

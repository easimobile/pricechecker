package com.easipos.pricechecker.util

import android.app.Activity
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.easipos.pricechecker.Easi
import com.easipos.pricechecker.tools.Preference

val languages = mapOf("zh" to "中文", "en" to "English")

fun defaultRequestOption(): RequestOptions {
    return RequestOptions()
}

fun ImageView.loadImage(imageUrl: String) {
    Glide.with(this.context)
        .load(imageUrl)
        .apply(defaultRequestOption())
        .transition(DrawableTransitionOptions().crossFade(500))
        .into(this)
}

fun changeLanguage(activity: Activity, language: String) {
    if (Preference.prefLanguageCode != language) {
        Preference.prefLanguageCode = language
        Easi.localeManager.setNewLocale(activity, language)
        activity.recreate()
    }
}
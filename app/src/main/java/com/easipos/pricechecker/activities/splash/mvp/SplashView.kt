package com.easipos.pricechecker.activities.splash.mvp

import com.easipos.pricechecker.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}

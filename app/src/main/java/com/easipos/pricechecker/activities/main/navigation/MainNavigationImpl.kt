package com.easipos.pricechecker.activities.main.navigation

import android.app.Activity
import com.easipos.pricechecker.activities.settings.SettingsActivity

class MainNavigationImpl : MainNavigation {

    override fun navigateToSettings(activity: Activity) {
        activity.startActivity(SettingsActivity.newIntent(activity))
    }
}
package com.easipos.pricechecker.activities.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import com.easipos.pricechecker.R
import com.easipos.pricechecker.activities.main.mvp.MainPresenter
import com.easipos.pricechecker.activities.main.mvp.MainView
import com.easipos.pricechecker.activities.main.navigation.MainNavigation
import com.easipos.pricechecker.base.CustomBaseAppCompatActivity
import com.easipos.pricechecker.models.ProductPricing
import com.easipos.pricechecker.tools.Preference
import io.github.anderscheow.library.kotlinExt.*
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<MainNavigation>()

    private val presenter by lazy { MainPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun populateProductPricing(productPricing: ProductPricing) {
        layout_result.visible()

        text_view_price.text = productPricing.price.formatAmount()
        text_view_barcode.text = getString(R.string.label_barcode_placeholder, productPricing.barcode)
        text_view_pos.text = getString(R.string.label_pos_placeholder, productPricing.prodNm)

        if (productPricing.promoMsg.isBlank() || productPricing.promoMsg == "N/A") {
            image_view_promo.setImageResource(R.drawable.bg_no_promo)
            text_view_promo.text = getString(R.string.label_promo_placeholder, getString(R.string.label_na))
        } else {
            image_view_promo.setImageResource(R.drawable.bg_promo)
            text_view_promo.text = getString(R.string.label_promo_placeholder, productPricing.promoMsg)
        }

        delay(Preference.prefIdleTime.toLong() * 1000) {
            clearResult()
        }
    }

    override fun clearResult() {
        runOnUiThread {
            layout_result.invisible()

            edit_text_barcode.text = null
        }
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        layout_result.invisible()
    }

    private fun setupListeners() {
        image_view_settings.click {
            navigation.navigateToSettings(this)
        }

        edit_text_barcode.apply {
            this.showSoftInputOnFocus = false
            this.setOnKeyListener { _, keyCode, event ->
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    val barcode = edit_text_barcode.text?.trim()?.toString() ?: ""
                    if (barcode.isNotBlank()) {
                        presenter.getProductPricing(barcode)
                    }
                    return@setOnKeyListener true
                }
                false
            }
            this.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus.not()) {
                    v.requestFocus()
                }
            }
        }
    }
    //endregion
}

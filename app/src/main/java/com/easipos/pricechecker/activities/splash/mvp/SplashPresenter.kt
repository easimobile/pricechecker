package com.easipos.pricechecker.activities.splash.mvp

import android.app.Application
import com.easipos.pricechecker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.pricechecker.base.Presenter
import com.easipos.pricechecker.managers.UserManager
import com.easipos.pricechecker.models.Result
import com.easipos.pricechecker.repositories.precheck.PrecheckRepository
import com.easipos.pricechecker.tools.Preference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class SplashPresenter(application: Application)
    : Presenter<SplashView>(application) {

    private val precheckRepository by instance<PrecheckRepository>()

    fun checkVersion() {
        launch {
            val result = precheckRepository.checkVersion(CheckVersionRequestModel())
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<Boolean> -> {
                        if (result.data) {
                            view?.showUpdateAppDialog()
                        } else {
                            checkIsAuthenticated()
                        }
                    }

                    is Result.Error -> {
                        checkIsAuthenticated()
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun checkIsAuthenticated() {
        if (Preference.prefIsLoggedIn && UserManager.token != null) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}


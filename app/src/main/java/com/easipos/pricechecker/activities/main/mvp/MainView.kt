package com.easipos.pricechecker.activities.main.mvp

import com.easipos.pricechecker.base.View
import com.easipos.pricechecker.models.ProductPricing

interface MainView : View {

    fun populateProductPricing(productPricing: ProductPricing)

    fun clearResult()
}

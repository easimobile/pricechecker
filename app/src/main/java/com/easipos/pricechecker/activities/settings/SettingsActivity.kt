package com.easipos.pricechecker.activities.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.pricechecker.R
import com.easipos.pricechecker.activities.settings.mvp.SettingsPresenter
import com.easipos.pricechecker.activities.settings.mvp.SettingsView
import com.easipos.pricechecker.activities.settings.navigation.SettingsNavigation
import com.easipos.pricechecker.base.CustomBaseAppCompatActivity
import com.easipos.pricechecker.fragments.alert_dialog.CommonAlertDialog
import com.easipos.pricechecker.tools.Preference
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.NotBlankRule
import io.github.anderscheow.validator.rules.common.RegexRule
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.longToast
import org.kodein.di.generic.instance
import kotlin.system.exitProcess

class SettingsActivity : CustomBaseAppCompatActivity(), SettingsView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SettingsActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<SettingsNavigation>()
    private val validator by instance<Validator>()

    private val presenter by lazy { SettingsPresenter(application) }

    private val urlRegex = "https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)"
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_settings

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog.show(this, message, action)
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        val url = Preference.prefCustomBaseUrl
        if (url.isNotBlank()) {
            text_input_edit_text_url.setText(url)
        } else {
            text_input_edit_text_url.setText("http://")
        }

        text_input_edit_text_idle_time.setText(Preference.prefIdleTime.toString())
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        text_view_save.click {
            attemptSaveSettings()
        }
    }

    private fun attemptSaveSettings() {
        val currentUrl = Preference.prefCustomBaseUrl

        val urlValidation = text_input_layout_url.validate()
            .add(NotBlankRule(R.string.error_field_required))
            .add(RegexRule(urlRegex, R.string.error_url_format))

        val idleTimeValidation = text_input_layout_idle_time.validate()
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val url = values[0].trim()
                val idleTime = values[1].trim().toIntOrNull() ?: 0

                if (url.endsWith("/")) {
                    Preference.prefCustomBaseUrl = url
                } else {
                    Preference.prefCustomBaseUrl = "$url/"
                }

                Preference.prefIdleTime = idleTime

                if (currentUrl == url) {
                    showErrorAlertDialog(getString(R.string.prompt_settings_save_successfully)) {
                        finish()
                    }
                } else {
                    showErrorAlertDialog(getString(R.string.prompt_url_changes)) {
                        exitProcess(0)
                    }
                }
            }
        }).validate(urlValidation, idleTimeValidation)
    }
    //endregion
}

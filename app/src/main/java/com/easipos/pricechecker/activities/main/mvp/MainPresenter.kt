package com.easipos.pricechecker.activities.main.mvp

import android.app.Application
import com.easipos.pricechecker.base.Presenter
import com.easipos.pricechecker.models.ProductPricing
import com.easipos.pricechecker.models.Result
import com.easipos.pricechecker.repositories.checker.CheckerRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class MainPresenter(application: Application)
    : Presenter<MainView>(application) {

    private val checkerRepository by instance<CheckerRepository>()

    fun getProductPricing(barcode: String) {
        view?.setLoadingIndicator(true)
        launch {
            val result = checkerRepository.getProductPricing(barcode)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<ProductPricing> -> {
                        view?.setLoadingIndicator(false)
                        view?.populateProductPricing(result.data)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.clearResult()
                    }

                    else -> {
                    }
                }
            }
        }
    }
}

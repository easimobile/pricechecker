package com.easipos.pricechecker.activities.settings.mvp

import android.app.Application
import com.easipos.pricechecker.base.Presenter

class SettingsPresenter(application: Application)
    : Presenter<SettingsView>(application)

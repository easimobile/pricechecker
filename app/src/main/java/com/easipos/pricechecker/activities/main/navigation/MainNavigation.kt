package com.easipos.pricechecker.activities.main.navigation

import android.app.Activity

interface MainNavigation {

    fun navigateToSettings(activity: Activity)
}
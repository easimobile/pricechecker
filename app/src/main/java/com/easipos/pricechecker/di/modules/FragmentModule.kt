package com.easipos.pricechecker.di.modules

import io.github.anderscheow.library.di.modules.FragmentBaseModule
import org.kodein.di.Kodein

class FragmentModule : FragmentBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
    }
}

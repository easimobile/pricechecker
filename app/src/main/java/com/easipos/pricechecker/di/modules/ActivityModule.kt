package com.easipos.pricechecker.di.modules

import com.easipos.pricechecker.activities.main.navigation.MainNavigation
import com.easipos.pricechecker.activities.main.navigation.MainNavigationImpl
import com.easipos.pricechecker.activities.splash.navigation.SplashNavigation
import com.easipos.pricechecker.activities.splash.navigation.SplashNavigationImpl
import io.github.anderscheow.library.di.modules.ActivityBaseModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

class ActivityModule : ActivityBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<SplashNavigation>() with provider { SplashNavigationImpl() }
            bind<MainNavigation>() with provider { MainNavigationImpl() }
        }
    }
}
